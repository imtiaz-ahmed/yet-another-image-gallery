'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var shell = require('gulp-shell');

// Task for compiling sass
gulp.task('sass', function () {
  return gulp.src('./assets/scss/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

// Task for watching sass folder for immidiate compilation
gulp.task('sass:watch', function () {
  gulp.watch('./assets/scss/*.scss', ['sass']);
});

// Task for starting Live Development Server
gulp.task('live', shell.task([
  "browser-sync start --server '.' --files 'app,assets/css,views,index.html'"
]));

// Task for creating zip for publishing
gulp.task('zip', shell.task([
  "zip -r app.zip app assets node_modules index.html views"
]));
