app.controller('flickrController', ['$scope', '$flickrService', 'Lightbox', '$window', function ($scope, $flickrService, Lightbox, $window) {

  //Initialise variables
  var list = [];
  $scope.sortType = "";
  $scope.sortBy = 'published';
  $scope.sendingEmail = false;


  // Function to get images from Flickr with tag
  $scope.getImage = function (tag) {
    $scope.loading = true; // Enable Loading Screen
    $flickrService.getPublicFeed(tag).then(function (response) {
      if (response.data && response.data.items) {
        list = response.data.items;
        $scope.imageList = angular.copy(list); // Deep copy of var list, so we can use list anywhere else later if required.
        console.log(list);
      } else {
        console.log(response);
        alert("Something went wrong. Check Console log")
      }
      $scope.loading = false; // Disable Loading Screen
    }, function (error) {
      console.log(error);
      alert("Invalid json data from Flickr. Check Console log");
      $scope.loading = false; // Disable Loading Screen
    })
  };

  // Show images from Flickr with tag 'day' after view loaded.
  $scope.$on('$viewContentLoaded', function (event) {
    $scope.getImage('unsplash'); //Setting image tags for First load.
  });

  // Function for Image sorting
  $scope.getSortBy = function (sortBy, sortType) {
    if ($scope.sortBy && $scope.sortType == '-') {
      return '-' + $scope.sortBy;
    } else {
      return $scope.sortBy;
    }
  };

  //Open Image in Lightbox
  $scope.openLightboxModal = function (image) {
    var indexOfImage = $scope.imageList.indexOf(image); // To get correct location of Image after Sorting
    Lightbox.openModal($scope.imageList, indexOfImage);
  };

  //Download Image
  $scope.downloadImage = function (imageUrl) {
    $window.open(imageUrl.replace('_b.jpg', '_b_d.jpg'));
  };

  //Share via email
  $scope.shareViaEmail = function (emailData, imageUrl) {
    emailData.imgUrl = imageUrl;
    $scope.sendingEmail = true;
    $flickrService.shareViaEmail(emailData).then(function (res) {
      console.log(res);
      $scope.sendingEmail = false;
      if (res.data.result) {
        $scope.sendStatus = res.data.message;
        $scope.showForm = false;
      }
    }, function (err) {
      console.log(err)
      $scope.sendingEmail = false;
    }).catch(function (error) {
      console.log(error);
      $scope.sendingEmail = false;
    })
  }
}]);