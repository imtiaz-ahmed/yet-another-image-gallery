app.factory('$flickrService', ['$http','appSettings', function($http,appSettings){
  
  return{
      getPublicFeed: function(tags){
          return $http.post(appSettings.api_url+ '/get-feed',
            {
              tags: tags
            },
            {
              cache: true //Enabling Image Cache
            });
      },
      shareViaEmail: function(emailData){
          return $http.post(appSettings.api_url + '/share-via-email',emailData);
      }
    }
}]);
