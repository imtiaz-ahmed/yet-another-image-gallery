var app = angular.module('YetAnotherImageGallery', ['ui.router', 'ngSanitize','bootstrapLightbox']);

app.config(['$stateProvider', '$urlRouterProvider','LightboxProvider',
  function($stateProvider, $urlRouterProvider,LightboxProvider) {
            $urlRouterProvider.otherwise('/');

            $stateProvider
            .state('/', {
                    url: '/',
                    templateUrl: 'views/home.html',
                    controller: 'flickrController'
                })
                //custom lightbox
              LightboxProvider.templateUrl = 'views/partials/lightbox.html';
              LightboxProvider.getImageUrl = function (image) {
                return image.media.m.replace('_m.jpg', '_b.jpg'); //replace medium size image url with original size
              };

              LightboxProvider.getImageCaption = function (image) {
              return image.description;
            };

        }]);

app.constant('appSettings',{
"api_url" : "http://localhost:9000"
});
