const express = require('express'),
  bodyParser = require('body-parser'),
  unirest = require('unirest'),
  app = express(),
  handlebars = require('handlebars'),
  fs = require('fs'),
  nodemailer = require('nodemailer');

let constants = {
  api_url: "https://api.flickr.com/services/feeds/photos_public.gne?nojsoncallback=1?&format=json&tags=",
  port: 9000
};

/****************************
Express Configuration
****************************/
app.use(bodyParser.urlencoded({
  extended: true
})); //middleware to parse URL-encoded body.
app.use(bodyParser.json()); //middleware to parse JSON.
app.use(function (mountPath, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Headers', 'accept, content-type');
  return next();
});

/****************************
Global Functions
****************************/

// HTTP_GET is a function for any REST API GET call
let HTTP_GET = function (url, next) {
  unirest.get(url)
    .headers({
      'Content-Type': 'application/json'
    })
    .end(function (response) {
      var body = response.body;
      if (next) next(body);
    });
};

/****************************
Email Configuration
****************************/

// Create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport('smtps://hello@pinrotech.com:Reset@pinro1234@smtp.gmail.com');
let mailOptions = {
  from: '"Yet Another Image Gallery" <hello@pinrotech.com>', // sender address
  text: 'Please enable HTML to view email contents' // plaintext body
};

//Function to read html file
let readHTMLFile = function (path, callback) {
  fs.readFile(path, {
    encoding: 'utf-8'
  }, function (err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};
/****************************
Endpoints
****************************/

// Root Endpoint
app.get('/', function (req, res) {
  res.status(200)
    .send('<div style="text-align:center"><img src="https://i.imgsafe.org/3a74550768.jpg"><br><h1>Congrats!</h1> <h3>You find your way to the Back Room<br>of Yet Another Image Gallery.</h3></div>');
  return next();
});

// Endpoint to get images from Flickr with/without Tags.
// Endpoint expects to have body data as JSON object. i.e. {"tags": "dog"}
app.post('/get-feed', function (req, res) {

  var data = req.body;
  var tags = data.tags;

  // Send error response if there is no Tag data
  if (!data || !tags) {
    res.status(406).send({
      result: false,
      message: "tags required"
    })
  } else {

    // Get Flickr images using HTTP_GET function passing url
    HTTP_GET(constants.api_url + tags, function (dataToSend) {
      // If invalid response from Flickr send Error.
      if (dataToSend && typeof dataToSend == 'string') {
        res.status(404).send({
          result: false,
          message: "Invalid json data from Flickr"
        });
        return;
      }
      if (dataToSend && dataToSend.items) {
        //Cleaning up JSON response from Flickr.
        dataToSend.items.map(item => {
          item.author = item.author.replace(/["]/g, ''); // Deleting Extra quote from author Name
          item.author = item.author.replace(/[(]/g, '['); // Replacig () with [] from author Name
          item.author = item.author.replace(/[)]/g, ']'); // Replacig () with [] from author Name
          item.description = item.description.replace(/ <p>/g, "<p>"); // Replacig extra space before p tag from description
        })
      }
      res.send(dataToSend);
    });
  }
});


// Endpoint to send email with images.
// Endpoint expects to have body data as JSON object. i.e. {"name" : "John", "email" : "imtiaz@pinrotech.com", "imgUrl" : "http://urloftheimage.png"}
app.post('/share-via-email', function (req, res) {

  let data = req.body;
  if (!data.name && !data.email && !data.imgUrl) {
    res.status(400).send({
      result: false,
      message: "Name, recipient's email and image url required"
    });
    return;
  }

  // Read HTML Email Templates and Pass data from Endpoint.
  readHTMLFile(__dirname + '/_partials/send-image.html', function (err, html) {
    var template = handlebars.compile(html);
    var replacements = {
      data: data
    };
    var htmlToSend = template(replacements); // to render in email template
    //structure email options
    mailOptions.html = htmlToSend;
    mailOptions.to = data.email;
    mailOptions.subject = `${data.name} has shared an image with you. 💖`;
    //send email
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        res.status(404).send({
          result: false,
          message: "Email sending failed. Please try again. "
        });
        return;
      }
      res.send({
        result: true,
        message: "Email sent successfully"
      });
      console.log('Message sent: ' + info.response);
    }); //send email ends

  }); //read html ends


});

/****************************
HTTP Server
****************************/

var httpServer = require('http').createServer(app);

httpServer.listen(constants.port, function () {
  console.log('Server for YAIG v2.0 running on port ' + constants.port + '.');
});