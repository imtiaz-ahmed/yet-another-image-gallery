![Yet another Image Gallery](https://i.imgsafe.org/3a74550768.jpg "Version 1.0.0")
###### Version 1.0.0 ######

Create an image gallery application that uses the public image Flickr feed as a data source. Use git as the source code version control system.

------------------------------------------------------------------------



##### Mandatory Requirements #####
* Use the latest development tools promoted by the platform you are working on  ![](https://i.imgsafe.org/3a972c191d.png)
* The endpoint to request images is [ https://www.flickr.com/services/feeds/docs/photos_public ] ![](https://i.imgsafe.org/3a972c191d.png)
* UI/UX for browsing using the Photo Gallery is at your discretion  ![](https://i.imgsafe.org/3a972c191d.png)
* Image metadata must be visible for each picture. ![](https://i.imgsafe.org/3a972c191d.png)
* Git is used to track the application development  ![](https://i.imgsafe.org/3a972c191d.png)

##### Secondary Requirements #####

* Search for images by tag ![](https://i.imgsafe.org/3a972c191d.png)
* Image caching ![](https://i.imgsafe.org/3a972c191d.png)
* Order by date taken or date published ![](https://i.imgsafe.org/3a972c191d.png)
* Save image to the System Gallery ![](https://i.imgsafe.org/3a972c191d.png)
* Open image in system browser ![](https://i.imgsafe.org/3a972c191d.png)
* Share picture by email ![](https://i.imgsafe.org/3a972c191d.png)


### Instructions for Front-end ###
-----------------------------------------
Install all the dependancy using node package manager.
```
npm install
```
To run the app with Live Development server. Please install browser-sync globally before running command below.
```
npm install -g browser-sync
```
```
gulp live
```
For creating zip folder to publish use command below:
```
gulp zip
```

### Instructions for Back-end ###
-----------------------------------------
Install all the dependancy using node package manager.
```
npm install
```
To run the node server, while in the server folder type following
```
npm start
```

### Credits ###
---
* [AngularUI Bootstrap Modal By Chris Lui][1].
* [Image Search Gif By Pantufla Cuántica][2].
* [Spinner Gif By  William Newton][3].

[1]: https://github.com/compact/angular-bootstrap-lightbox
[2]: https://dribbble.com/shots/3101872-Searching-concepts
[1]: https://dribbble.com/shots/2347771-Glowing-Growing-Spinner-GIF
